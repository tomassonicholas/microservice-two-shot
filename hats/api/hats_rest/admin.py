from django.contrib import admin

from .models import Size, Style
# Register your models here.
@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    pass

@admin.register(Style)
class StyleAdmin(admin.ModelAdmin):
    pass
