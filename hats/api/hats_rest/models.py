from django.db import models
from django.urls import reverse
# Create your models here.


class Size(models.Model):
    name = models.CharField(max_length=5, unique=True)

    def __str__(self) -> str:
        return self.name


class Style(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self) -> str:
        return self.name


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self) -> str:
        return self.closet_name


class Hat(models.Model):
    brand = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    fabric = models.CharField(max_length=50)
    img_href = models.URLField()
    style = models.ForeignKey(
        Style,
        related_name='hat',
        on_delete=models.PROTECT,
    )
    size = models.ForeignKey(
        Size,
        related_name='hat',
        on_delete=models.PROTECT,
    )
    location = models.ForeignKey(
        LocationVO,
        related_name='hat',
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return '{self.color} {self.brand} hat'

    def get_api_url(self):
        return reverse('api_show_hat', kwargs={'pk': self.pk})
