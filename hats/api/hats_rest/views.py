from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO, Size, Style


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        'brand',
        'color',
        'fabric',
        'img_href',
        'id',
    ]

    def get_extra_data(self, o):
        return {
            'size': o.size.name,
            'style': o.style.name,
            'location': o.location.import_href,
        }


class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
        'section_number',
        'shelf_number',
    ]


class StyleEncoder(ModelEncoder):
    model = Style
    properties = ['name']


class SizeEncoder(ModelEncoder):
    model = Size
    properties = ['name']


@require_http_methods(['GET','POST', 'DELETE'])
def api_post_locations(request):
    if request.method == 'GET':
        locations = LocationVO.objects.all()
        return JsonResponse(
            {'locations': locations},
            encoder=LocationEncoder,
        )
    elif request.method == 'DELETE':
        content = json.loads(request.body)
        count, _ = LocationVO.objects.filter(import_href=content['import_href']).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        content = json.loads(request.body)
        location = LocationVO.objects.update_or_create(**content)
        return JsonResponse(
            location,
            encoder=LocationEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'POST'])
def api_list_hats(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            content['size'] = Size.objects.get(name=content['size'])
            content['style'] = Style.objects.get(name=content['style'])
            content['location'] = LocationVO.objects.get(import_href=content['location'])
        except:
            return JsonResponse({
                    'Error': 'Issue with request content',
                    str(content.keys()): 'should be: "color", "brand", "size" "style", "location"',
                }
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_hat_details(request, id):
    if request.method == 'GET':
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count,_ = Hat.objects.filter(id=id).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            content['size'] = Size.objects.get(name=content['size'])
            content['style'] = Style.objects.get(name=content['style'])
        except:
            return JsonResponse(
                {'Error': 'Invalid Size or Style'}
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(['GET'])
def api_list_styles(request):
    if request.method == 'GET':
        styles = Style.objects.all()
        return JsonResponse(
            {'styles': styles},
            encoder=StyleEncoder,
            safe=False,
        )


@require_http_methods(['GET'])
def api_list_sizes(request):
    if request.method == 'GET':
        sizes = Size.objects.all()
        return JsonResponse(
            {'sizes': sizes},
            encoder=SizeEncoder,
            safe=False,
        )
