from django.urls import path
from .views import (
    api_post_locations,
    api_list_hats,
    api_hat_details,
    api_list_sizes,
    api_list_styles,
)

urlpatterns = [
    path('locations/', api_post_locations, name="api_post_locations"),
    path('', api_list_hats, name='api_list_hats'),
    path('<int:id>/', api_hat_details, name='api_hat_details'),
    path('sizes/', api_list_sizes, name='api_list_sizes'),
    path('styles/', api_list_styles, name='api_list_styles'),
]
