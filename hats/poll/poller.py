import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


# Import models from hats_rest, here.
# from hats_rest.models import Something
def get_locations():
    get_response = requests.get('http://wardrobe-api:8000/api/locations/')
    content = json.loads(get_response.content)
    # headers = {'content-type': 'appllication/json'}
    for location in content['locations']:
        data = {
            'import_href' :location['href'],
            'closet_name': location['closet_name'],
            'section_number': location['section_number'],
            'shelf_number': location['shelf_number'],
        }
        try:
            post_req = requests.post('http://hats-api:8000/hats/locations/', json=data)
        except Exception:
            pass



def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
