# Wardrobify

Team:

* Nick Tomasso - Hats MicroService
* Person 2 - Which microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I created a model to represent hats with the required attributes of fabric and color as CharFields, the img url as a URLField and the style as
a foreign Key to a Style models so I could preload a set of hat styles rather than have the user type a style each time, I thought about doing the same for color and
fabric as there are finite variations of each but decided against it as people may want to be more or less specific about these attributes, I also added a brand and size
attribute brand being a charField and size being a foreign key to a preset model of sizes from XS to XL for consistancy of input, and a locationVO was also foreign keyed to the
hat model, locations not preloaded instead provided by a pooling service that periodically pings the wardrobe api.

The poller exists in its own file structure outside of the hats api so i decided to write the poller to make a get request to the wardrobe api grabbing all wardrobe objects and then
send these objects via a post request to the hats api where my Location List API view calls an update or create method on the LocationVO ensuring that all LocationVOs are always
up to date with the Wardrobe db.

In the Front end I created 2 pages a list view of all hats where i fetch my Hats List API and then call a fetch on each hats location href to import the properties of each hat and its
individual loaction attibutes so the user can see exactly where in the wardrobe wach hat is.  At the bottom of this page I added a Button with a link to the Create a Hat page to add
new hats to the db, as well as a button at the end of each list item to delete a hat from the db, and then update the state to dynamically redraw the inventory table.

For the hat creation page i went with a simple card layout with floating form fields to input the Brand, Fabric, Color, and Image URL, and three propdown tables to select size, style, and
location all linked to the formData state and a button on the bottom of the card to create the new hat in the db with a custom onSubmit function.  THe sizes, locations, and styles are all
pulled into the state via api calls to their respective api list views on the backend.

NOTE: When loading this application for the first time please ensure you use the django shell or the admin panel to create the preloaded sizes and styles of hats into the db to maintain
functionality.
