import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateHat from './components/hats/CreateHat';
import ListHats from './components/hats/HatsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='hats/' element={<ListHats />}/>
          <Route path="hats/new" element={<CreateHat />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
