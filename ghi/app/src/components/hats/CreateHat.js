import React, { useEffect, useState } from 'react'

function CreateHat () {
    const [styles, setStyles] = useState([])
    const [sizes, setSizes] = useState([])
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        brand: '',
        fabric: '',
        img_href: '',
        size: '',
        style: '',
        location: '',
    })

    const fetchData = async () => {
        const styleUrl = 'http://localhost:8090/hats/styles/'
        const sizeUrl = 'http://localhost:8090/hats/sizes/'
        const locationUrl = 'http://localhost:8090/hats/locations/'

        const styleResponse = await fetch(styleUrl)
        const sizeResponse = await fetch(sizeUrl)
        const locationResponse = await fetch(locationUrl)

        const styleDetail = await styleResponse.json()
        const sizeDetail = await sizeResponse.json()
        const locationDetail = await locationResponse.json()

        setStyles(styleDetail.styles)
        setSizes(sizeDetail.sizes)
        setLocations(locationDetail.locations)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const hatUrl = 'http://localhost:8090/hats/'
        console.log(JSON.stringify(formData))
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(hatUrl, fetchConfig)
        console.log(response)
        if ( response.ok ) {
            const newHat = await response.json()
            console.log(newHat)
            setFormData({
                color: '',
                brand: '',
                fabric: '',
                img_href: '',
                size: '',
                style: '',
                location: '',
            })
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const name = event.target.name
        setFormData({...formData, [name]: value})
    }


    return (
        <div className="row">
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add a new hat</h1>
                    <form onSubmit={handleSubmit} id='create-hat-form' className='form-floating row g-3'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.color} className='form-control' id='color' name='color' type='text' placeholder='ROYGBIV' />
                            <label htmlFor='inputColor'>Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.brand} className='form-control' id='brand' name='brand' type='text' placeholder='AdidYasss' />
                            <label htmlFor='inputBrand'>Brand</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.fabric} className='form-control' id='fabric' name='fabric' type='text' placeholder='Cotton' />
                            <label htmlFor='inputBrand'>Fabric</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.img_href} className='form-control' id='img_href' name='img_href' type='text' placeholder='awesomehats.com' />
                            <label htmlFor='inputBrand'>Image URL</label>
                        </div>
                        <div className='col-md-6 mb-3'>
                            <select onChange={handleFormChange} value={formData.size} required name='size' id='size' className="form-select">
                                <option>Size</option>
                                {sizes.map(size => {
                                    return (
                                        <option value={size.name} key={size.name}>
                                            {size.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className='col-md-6 mb-3'>
                            <select onChange={handleFormChange} value={formData.style} required name='style' id='style' className="form-select">
                                <option>Style</option>
                                {styles.map(style => {
                                    return (
                                        <option value={style.name} key={style.name}>
                                            {style.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className='mb-3'>
                            <select onChange={handleFormChange} value={formData.location} required name='location' id='location' className="form-select">
                                <option>Location</option>
                                {locations.map(location => {
                                    return (
                                        <option value={location.import_href} key={location.import_href}>
                                            Closet: {location.closet_name} | Section: {location.section_number} | Shelf: {location.shelf_number} |
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div>
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateHat
