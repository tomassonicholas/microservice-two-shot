import { useEffect, useState } from "react"
import CreateHat from "./CreateHat"
import { Link } from "react-router-dom"


function ListHats () {
    const [hats, setHats] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/hats/'

        const response = await fetch(url)

        const data = await response.json()

        try {
            for ( let hat of data.hats ) {

                const locationUrl = `http://localhost:8100${hat.location}`
                const detailsResponse = await fetch(locationUrl)
                const details = await detailsResponse.json()
                hat['closet'] = details.closet_name
                hat['section'] = details.section_number
                hat['shelf'] = details.shelf_number
            }
            setHats(data.hats)
        } catch (e) {
            console.error()
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = async (event) => {
        const url = `http://localhost:8090/hats/${event.target.value}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application.json',
            },
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            const deleted = await response.json()
            console.log(deleted)

            fetchData()
        }

    }

    const handleRedirect = (event) => {
        event.preventDefault()
        window.history.pushState(hats, 'http://localhost:3000/hats')
        const target = event.target.innerHTML
        window.location.replace(target)
    }

    return (
        <div className="container mt-3">
            <div className="shadow card">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th key={'Brand'} scope="col">Brand</th>
                            <th key={'Color'} scope="col">Color</th>
                            <th key={'Fabric'} scope="col">Fabric</th>
                            <th key={'Picture'} scope="col">Picture</th>
                            <th key={'Size'} scope="col">Size</th>
                            <th key={'Style'} scope="col">Style</th>
                            <th key={'Closet'} scope="col">Closet</th>
                            <th key={'Section'} scope="col">Section</th>
                            <th key={'Shelf'} scope="col">Shelf</th>
                            <th key={'Actions'} scope='col'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {hats.map(hat => {
                            return (
                                <tr key={`row for hat ${hat.id}`}>
                                    <td key={`hat ${hat.id} brand`}>{hat.brand}</td>
                                    <td key={`hat ${hat.id} color`}>{hat.color}</td>
                                    <td key={`hat ${hat.id} fabric`}>{hat.fabric}</td>
                                    <td key={`hat ${hat.id} img`}><Link onClick={handleRedirect} to={''}>{hat.img_href}</Link></td>
                                    <td key={`hat ${hat.id} size`}>{hat.size}</td>
                                    <td key={`hat ${hat.id} style`}>{hat.style}</td>
                                    <td key={`hat ${hat.id} closet`}>{hat.closet}</td>
                                    <td key={`hat ${hat.id} section`}>{hat.section}</td>
                                    <td key={`hat ${hat.id} shelf`}>{hat.shelf}</td>
                                    <td key={hat.id}><button key={`hat ${hat.id} action button`} onClick={handleDelete} value={hat.id} className="btn btn-danger">Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <button className="btn btn-primary">
            <Link to={'new'} className="btn-primary">New</Link>
            </button>
        </div>
    )
}

export default ListHats
